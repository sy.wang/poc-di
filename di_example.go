package main

import (
	"database/sql"
	"encoding/json"
	"net/http"

	_ "github.com/mattn/go-sqlite3"
	"go.uber.org/dig"
)

type Config struct {
	Enabled      bool
	DatabasePath string
	Port         string
}

func NewConfig() *Config {
	return &Config{
		Enabled:      true,
		DatabasePath: "./ppl.db",
		Port:         "8000",
	}
}

type Person struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
	Age  int    `json:"age"`
}

type PersonRepo struct {
	database *sql.DB
}

func (repo *PersonRepo) FindAll() []*Person {
	rows, _ := repo.database.Query(`SELECT id, name, age FROM people;`)
	defer rows.Close()

	people := []*Person{}

	for rows.Next() {
		var (
			id   int
			name string
			age  int
		)

		rows.Scan(&id, &name, &age)

		people = append(people, &Person{
			Id:   id,
			Name: name,
			Age:  age,
		})
	}

	return people
}

func NewPersonRepo(db *sql.DB) *PersonRepo {
	return &PersonRepo{database: db}
}

type PersonService struct {
	config *Config
	repo   *PersonRepo
}

func (service *PersonService) FindAll() []*Person {
	if service.config.Enabled {
		return service.repo.FindAll()
	}
	return []*Person{}
}

func NewPersonService(config *Config, repo *PersonRepo) *PersonService {
	return &PersonService{config: config, repo: repo}
}

type Server struct {
	config        *Config
	personService *PersonService
}

func (server *Server) Handler() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/people", server.findPeople)

	return mux
}

func (server *Server) Run() {
	httpServer := &http.Server{
		Addr:    ":" + server.config.Port,
		Handler: server.Handler(),
	}
	httpServer.ListenAndServe()
}

func (server *Server) findPeople(writer http.ResponseWriter, request *http.Request) {
	people := server.personService.FindAll()
	bytes, _ := json.Marshal(people)

	writer.Header().Set("Content-Type", "applications/json")
	writer.WriteHeader(http.StatusOK)
	writer.Write(bytes)
}

func NewServer(config *Config, service *PersonService) *Server {
	return &Server{
		config:        config,
		personService: service,
	}
}

func ConnectDb(config *Config) (*sql.DB, error) {
	return sql.Open("sqlite3", config.DatabasePath)
}

// BuildContainer uses dig to create dependencies
func BuildContainer() *dig.Container {
	container := dig.New()

	container.Provide(NewConfig)
	container.Provide(ConnectDb)
	container.Provide(NewPersonRepo)
	container.Provide(NewPersonService)
	container.Provide(NewServer)

	return container
}

func main() {
	err := BuildContainer().Invoke(func(server *Server) {
		server.Run()
	})

	if err != nil {
		panic(err)
	}
}

// The manual way
//
// func main() {
// 	config := NewConfig()
//
// 	db, err := ConnectDatabase(config)
//
// 	if err != nil {
// 		panic(err)
// 	}
//
// 	personRepository := NewPersonRepository(db)
//
// 	personService := NewPersonService(config, personRepository)
//
// 	server := NewServer(config, personService)
//
// 	server.Run()
// }
