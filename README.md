## Populate the database
There should already be a file named `ppl.db` which contains database content.
Update `init_db.sh` and run `./init_db.sh` if you want to add more content to the database.

## Build the binary
```shell
go build
```

## Start the server
```shell
./poc-di
```

## Send a request
```shell
curl -v localhost:8000/people
```

## Example output response after request sent
```
sy.wang@F2L0GF3HWP poc-di % curl -v localhost:8000/people              

*   Trying 127.0.0.1:8000...
* Connected to localhost (127.0.0.1) port 8000 (#0)
> GET /people HTTP/1.1
> Host: localhost:8000
> User-Agent: curl/8.1.2
> Accept: */*
> 
< HTTP/1.1 200 OK
< Content-Type: applications/json
< Date: Fri, 18 Aug 2023 20:22:59 GMT
< Content-Length: 69
< 
* Connection #0 to host localhost left intact
[{"id":1,"name":"victoria","age":28},{"id":2,"name":"abby","age":47}]  
```

## Dependency in this example
A -> B means A depends on B  

`Config` does not depend on anything  
`ConnectDatabase` -> `Config`  
`PersonRepo` -> `*sql.DB`/`ConnectDatabase`  
`PersonService` -> `Config` & `PersonRepo`  
`Server` -> `Config` & `PersonService`

### Using Dig
```
container := dig.New()

// Two different ways to add to the container
// method 1: recommended
container.Provide(NewConfig)

// method 2
container.Provide(func() *Config {
    return NewConfig()
})
```
`container.Provide()` can be used to add all the dependencies and then `Invoke` gives a fully-built component for any
given type:
```
container.Invoke(func(ANY_TYPE_GOES_HERE) {
})
```
For example:
```
container.Invoke(func(db *sql.DB) {
    // *sql.DB is ready to use here
})
```
Internally, when `Invoke` is called:
1. The container recognizes that `*sql.DB` is needed
2. It knows that `ConnectDatabase` returns type `*sql.DB`
3. It finds that `ConnectDatabase` depends on `Config`
4. The provider for `Config` is the `NewConfig` function, so `NewConfig` will be called
5. The result of `NewConfig` gets passed to `ConnectDatabase`
6. Then `ConnectDatabase` can successfully return a `*sql.DB`
7. The result with type `*sql.DB` gets passed to `Invoke`